package com.ticktacktoe;

import java.io.IOException;
public class PlayerMove {
    private enum Symbol {
        X,
        O
    }
    private Symbol symbol;

    private Point selectedPoint;

    PlayerMove(int x, int y/*, Symbol symbol*/) {
        this.symbol = symbol;
        IOException e = new IOException();
        try {
            if (x > 2 || x < 0 || y < 0 || y > 2) throw e;
            selectedPoint = new Point(x,y);
        }
        catch (IOException e1){
            System.out.println("Nieprawidłowy parametr!");
        }


    }
    Symbol getSymbol(){
        return symbol;
    }
    Point getPoint(){
        return selectedPoint;
    }
}
