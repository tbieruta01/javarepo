package com.ticktacktoe;

public class Point {
    private int x;
    private int y;
    Point(int x, int y){
        this.x = x;
        this.y = y;
    }
    Point(){

    }
    int getX(){
        return x;
    }
    int getY() {
        return y;
    }
    void setX(int x) {
        this.x = x;
    }
    void setY(int y) {
        this.y = y;
    }
    public String toString(){
        return ("X: " + x + " Y: " + y);
    }

}
