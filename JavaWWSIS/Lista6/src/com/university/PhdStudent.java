package com.university;


public class PhdStudent extends Student {
    private String thesisTopic;
    PhdStudent(String name, String surname, String semester, String field, String thesisTopic) {
        super(name, surname, semester, field);
        this.thesisTopic = thesisTopic;
    }

    void setThesisTopic(String thesisTopic) {
        this.thesisTopic = thesisTopic;
    }

    public String getThesisTopic() {
        return thesisTopic;
    }

    public String toString(){
        return "PhD Student("
                + " name: " + getName()
                + " surname: " + getSurname()
                + " semester: " + getSemester()
                + " field: " + getField()
                + "Thesis Topic: " + getThesisTopic()
                +")";
    }
}
