package com.university;


import java.util.ArrayList;


import static com.university.Main.scanner;

public class Lecturer extends Person {
    Lecturer(String name, String surname) {
        super(name, surname);
        subjects = new ArrayList<>();

    }
    private ArrayList<String> subjects;
    ArrayList<String> getSubjects(){
        return subjects;
    }
    void setSubjects(int subjectsValue){

        for(int i = 0; i < subjectsValue; i++) {
            System.out.println("Proszę podać nazwę przedmiotu:");
            subjects.add(scanner.next());
        }
    }
    void setSubjects(ArrayList<String> subjects) {
        this.subjects = subjects;
    }

    public String toString(){
        return "Lecturer( name: " + getName() + " surname: " + getSurname() + " Subjects: " + getSubjects() + ")";
    }
}
