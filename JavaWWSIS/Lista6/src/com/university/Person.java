package com.university;


public class Person {
    private String name;
    private String surname;
    Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
    void setName(String name) {
        this.name = name;
    }
    String getName() {
        return name;
    }
    void setSurname(String surname) {
        this.surname = surname;
    }
    String getSurname() {
        return surname;
    }
    public String toString() {
        return "Name: " + getName()
                + " Surname " + getSurname() + ".";
    }
}
