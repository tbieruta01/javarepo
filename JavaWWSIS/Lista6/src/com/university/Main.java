package com.university;

import java.util.ArrayList;

import java.util.Scanner;
import java.util.Random;

import com.figures.*;

public class Main {

    public static void main(String[] args) {

        optionSelectioner();

        Random randomGenerator = new Random();

        Circle circle = new Circle(randomGenerator.nextInt(100));
        Triangle triangle = new Triangle(
                randomGenerator.nextInt(100),
                randomGenerator.nextInt(100),
                randomGenerator.nextInt(100)
        );
        Square square = new Square(randomGenerator.nextInt(100));

        square.print();
        triangle.print();
        circle.print();


    }




    static Scanner scanner = new Scanner(System.in);
    static ArrayList<Student> students= new ArrayList<>();
    static ArrayList<PhdStudent> phdStudents = new ArrayList<>();
    static ArrayList<Lecturer> lecturers = new ArrayList<>();

    static void readBase() {
        System.out.println(students);
        System.out.println(phdStudents);
        System.out.println(lecturers);
    }

    static char selectOption() {
        System.out.println("Proszę wybrać osobę do utworzenia:");
        System.out.println("S: Student\nP: Doktorant\nL: Wykładowca\nR: Odczytaj \nE: Wyjdź");
        String selected = scanner.next();

        return selected.toUpperCase().charAt(0);
    }

    static void optionSelectioner(){
        boolean isActive = true;
        while(isActive) {
            switch (selectOption()) {
                case 'S':
                    System.out.println("Imię, Nazwisko, Semestr, Kierunek");
                    students.add(
                            new Student(

                                    scanner.next(), //name
                                    scanner.next(), //surname
                                    scanner.next(), //semester
                                    scanner.next()  //field

                            )
                    );
                    break;
                case 'P':
                    System.out.println("Imię, Nazwisko, Semestr, Kierunek, Nazwa Pracy");
                    phdStudents.add(
                            new PhdStudent(

                                    scanner.next(), //name
                                    scanner.next(), //surname
                                    scanner.next(), //semester
                                    scanner.next(), //field
                                    scanner.next()  //thesis topic

                            )
                    );
                    break;
                case 'L':
                    System.out.println("Imię, Nazwisko");
                    lecturers.add(
                            new Lecturer(
                                    scanner.next(),
                                    scanner.next()
                            )
                    );
                    System.out.println("Proszę podać liczbę przedmiotów:");
                    lecturers.get(lecturers.size() - 1).setSubjects(scanner.nextInt());
                    break;
                case 'R' :
                    readBase();
                    break;
                case 'E':
                    isActive = false;
                    break;
                default:
                    System.out.println("Podano nieprawidłową opcję!");
                    break;

            }
        }
    }



}
