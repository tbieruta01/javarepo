package com.university;


public class Student extends Person {
    private String semester;
    private String field;
    void setSemester(String semester){
        this.semester = semester;
    }
    String getSemester() {
        return semester;
    }
    void setField(String field) {
        this.field = field;
    }
    String getField() {
        return field;
    }
    Student(String name, String surname, String semester, String field) {
        super(name, surname);
        this.semester = semester;
        this.field = field;
    }
    public String toString() {
        return "Student( " +
                "name: " + getName()
                + " surname: " + getSurname()
                + " semester: " + getSemester()
                + " field: " + getField()
                + ")";
    }

}
