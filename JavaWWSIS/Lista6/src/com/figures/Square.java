package com.figures;


public class Square implements Figure {
    private double a;
    public Square(double a) {
        this.a = a;
    }
    @Override
    public double calculateArea() {
        return Math.pow(a,2);
    }

    @Override
    public double calculatePerimeter() {
        return a*4;
    }

    @Override
    public void print() {
        System.out.println("Pole Kwadratu: " + calculateArea() + ", obwód: " + calculatePerimeter());
    }
}
