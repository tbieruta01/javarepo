package com.figures;


public class Triangle implements Figure {
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    @Override
    public double calculatePerimeter() {
        return a+b+c;
    }

    @Override
    public double calculateArea() {
        double p = calculatePerimeter()/2;
        return Math.sqrt(
                (p*(p-a)*(p-b)*(p-c)) //Znaleziony wzór Herona na pole trójkąta różnobocznego
        );
    }

    @Override
    public void print() {
        System.out.println("Pole trójkąta: " + calculateArea() + ", obwód: " + calculatePerimeter());
    }
}
