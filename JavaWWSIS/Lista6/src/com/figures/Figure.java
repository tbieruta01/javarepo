package com.figures;


public interface Figure {
    double calculatePerimeter();
    double calculateArea();
    void print();
}
