package com.figures;


public class Circle implements Figure{
    private double r;
    public Circle(double r) {
        this.r = r;
    }
    @Override
    public double calculateArea() {
        return Math.PI * Math.pow(r,2);
    }

    @Override
    public double calculatePerimeter() {
        return 2* Math.PI * r;
    }
    public void print(){
        System.out.println("Pole koła: " + calculateArea() + ", obwód: " + calculatePerimeter());
    }
}
