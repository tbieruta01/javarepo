package Third;

import java.util.ArrayList;

public class Dealer {
    ArrayList<Car> cars = new ArrayList<>();
    public Dealer(ArrayList<Car> cars){
        this.cars = cars;
    }
    public double getCarPrice(Car car) throws noCarException {
        for(Car aCar : cars) {
            if(aCar.doesModelsEquals(car)){
                return aCar.getPrice();
            }
            throw new noCarException("Niestety, taki samochód nie istnieje.");
        }
        return -1.0;
    }
}
