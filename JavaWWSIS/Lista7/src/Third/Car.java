package Third;

public class Car {
    private String brand;
    private String model;
    private double price;
    public String getBrand(){
        return brand;
    }
    public String getModel(){
        return model;
    }
    public double getPrice() {
        return price;
    }

    public Car(String brand, String model, double price) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }
    public Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }
    public boolean doesModelsEquals(Car car){
        if((this.brand.equals(car.getBrand()))
                && (this.model.equals(car.getModel()))){
            return true;
        }
        return false;
    }
}
