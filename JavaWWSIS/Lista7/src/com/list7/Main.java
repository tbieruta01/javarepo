package com.list7;

import Third.Car;
import Third.Dealer;
import com.first.User;
import Third.noCarException;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
	    User user = new User();
	    //user.setAge();
        ArrayList<Car> cars= new ArrayList<>();
        cars.add(new Car("Toyota", "Yaris", 55302.33));
        cars.add(new Car("Skoda", "Octavia", 65645.45));
        cars.add(new Car("Alfa Romeo", "Julia", 435345.54));
        Dealer mirek = new Dealer(cars);
        System.out.println("Proszę podać \n-markę \n-model");
        try {
            System.out.println("Cena: " + mirek.getCarPrice(new Car(scanner.next(), scanner.next())));
        }catch (noCarException e) {
            System.out.println(e.getMessage());
        }


    }

}
