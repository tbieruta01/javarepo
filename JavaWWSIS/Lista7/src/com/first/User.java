package com.first;


import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class User {
    private String name;
    private String surname;
    private int age;
    Scanner scanner;

    public User(){
        scanner = new Scanner(System.in);
        setName();
        setSurname();
        setAge();
    }
    void setName (){
        System.out.println("Proszę podać imię:");
        name = scanner.next();
    }
    void setSurname() {
        System.out.println("Proszę podać nazwisko:");
        surname = scanner.next();
    }
    public void setAge() {
        System.out.println("Proszę podać wiek:");
        while (!scanner.hasNextInt()) {
            try {
                System.out.println("Proszę podać wiek:");
                age = scanner.nextInt();
                break;
            } catch (InputMismatchException e) {
                System.out.println("Proszę wprowadzić odpowiednią liczbę jako wiek!");
            }
        }
    }
}