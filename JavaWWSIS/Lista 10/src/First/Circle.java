package First;

public class Circle extends Form{
    int r;
    public Circle(int r) {
        this.r = r;
    }
    @Override
    public double calculateCircuit(){
        return 2* Math.PI * r;
    }
}
