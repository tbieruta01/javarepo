package com.list10;

import First.Circle;
import First.Square;
import First.Triangle;
import Second.Bank;
import Second.GetInvestmentInfo;

public class Main {

    public static void main(String[] args) {
        Circle circle = new Circle(55);
        Triangle triangle = new Triangle(3,4,5);
        Square square = new Square(6);
        System.out.println("Circle: " + circle.calculateCircuit());
        System.out.println("Square: " + square.calculateCircuit());
        System.out.println("Triangle: " + triangle.calculateCircuit());


        GetInvestmentInfo getInvestmentInfo = new GetInvestmentInfo();
        getInvestmentInfo.getInvestmentInterest();
    }
}
