package Second;

public interface Investment {
    double monthly();
    double quarterly();
    double halfyearly();
    double yearly();

}
