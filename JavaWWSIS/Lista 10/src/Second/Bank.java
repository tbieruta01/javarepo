package Second;
import java.util.Random;
public abstract class Bank {
    private String name;
    private String address;
    Random random = new Random();

    Investment investment = new Investment() {
        private double interests = ((((double)random.nextInt(99))+1)/1000);
        @Override
        public double monthly() {
            return interests;
        }

        @Override
        public double quarterly() {
            return (interests * Math.pow(1.1, 3));
        }

        @Override
        public double halfyearly() {
            return (interests * Math.pow(1.067,6));
        }

        @Override
        public double yearly() {
            return (interests * Math.pow(1.0399, 12));
        }
    };
    Bank(String name) {
        this.name = name;
        random = new Random();


    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public abstract void createAccount();
    @Override
    public String toString(){
        return "Bank: " + name + "\n Adres: " + address + "\n";
    }
}
