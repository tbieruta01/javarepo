package Second;

import java.util.ArrayList;
import java.util.Random;

public class GetInvestmentInfo {
    public ArrayList<Bank> avalibleBanks = new ArrayList<>();
    public GetInvestmentInfo(){
        avalibleBanks.add(new EastBank("EastBank"));
        avalibleBanks.add(new Zbank("Zbank"));
        avalibleBanks.add(new NarniaBank("NarniaBank"));
        Random random = new Random();
        for(Bank bank : avalibleBanks) {
            bank.setAddress("Wrocław, ul. Bankowa " + random.nextInt(200));
        }
    }
    public void getInvestmentInterest() {
        System.out.println("Oprocentowanie w bankach: ");
        for(Bank bank : avalibleBanks) {
            System.out.println(bank);
            System.out.println("Oprocenowanie:");
            System.out.println("Miesięczne : " + bank.investment.monthly());
            System.out.println("Kwartalnie: " + bank.investment.quarterly());
            System.out.println("Półrocznie: " + bank.investment.halfyearly());
            System.out.println("Rocznie: " + bank.investment.yearly());
        }
    }

}
