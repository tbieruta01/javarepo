package List;

import java.util.Arrays;
import java.util.Scanner;

public class Second {
    public void createUserArray() {
        System.out.println("Proszę podać ilość elementów: ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int arr[] = new int[size];
        for(int i = 0; i < size; i++) {
            System.out.println("Proszę podać wartość: ");
            arr[i] = scanner.nextInt();
        }
        Arrays.sort(arr);
        System.out.print("\n[");
        for(int element : arr) {
            System.out.print(element + ", ");
        }
        System.out.print("]\n");


    }
}
