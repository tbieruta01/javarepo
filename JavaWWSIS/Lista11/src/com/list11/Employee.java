package com.list11;

/**
 * Created by tomasz on 22.01.17.
 */
public abstract class Employee {
    String name;
    String surname;
    int salary;
    int workingDays;
    float bonusSalary;
}
