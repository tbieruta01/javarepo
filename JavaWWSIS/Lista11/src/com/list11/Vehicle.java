package com.list11;

/**
 * Created by tomasz on 22.01.17.
 */
public abstract class Vehicle {
    String brand;
    String model;
    String year;
    String vinNumber;
}
