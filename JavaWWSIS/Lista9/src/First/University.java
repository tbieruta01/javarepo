package First;


import java.util.HashSet;

public class University {
    String name;
    public HashSet<Course> coursesSet;
    public University(String name) {
        coursesSet = new HashSet<>();
        this.name = name;
    }
    public void appendToCoursesSet(Course course) {
        coursesSet.add(course);
    }
    @Override
    public String toString() {
        return name + "\n" + coursesSet;
    }

}
