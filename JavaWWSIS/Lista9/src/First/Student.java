package First;

public class Student extends Person{
    int semester;
    String field;

    public Student(String name, String surname, int semester, String field) {
        super(name, surname);
        this.semester = semester;
        this.field = field;
    }
    @Override
    public String toString() {
        return("-" + name + " " + surname + " " + semester + " semestr " + field + "\n");
    }
}
