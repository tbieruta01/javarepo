package First;

public class Teacher extends Person {
    public Teacher(String name, String surname){
        super(name, surname);

    }
    @Override
    public String toString(){
        return name + " " + surname;
    }
}
