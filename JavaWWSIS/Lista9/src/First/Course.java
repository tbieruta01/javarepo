package First;

import java.util.ArrayList;

public class Course {
    String name;
    String code;
    ArrayList<Student> studentsList;
    Teacher teacher;
    public Course (String name, Teacher teacher, String code) {
        this.name = name;
        this.teacher = teacher;
        this.code = code;
        studentsList = new ArrayList<>();
    }
    public void addToStudentsList(Student student) {
        studentsList.add(student);
    }
    @Override
    public String toString(){
        return(name + " , prowadzący " + teacher + "\n"
                + "Studenci: \n" + studentsList + "\n"
                );
    }
}
