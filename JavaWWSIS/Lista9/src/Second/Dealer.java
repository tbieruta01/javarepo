package Second;

import java.util.ArrayList;
import java.util.HashMap;

public class Dealer {
    HashMap<String, ArrayList<Car>> avalibleCars = new HashMap<>();
    public Dealer() {
        avalibleCars = new HashMap<>();
    }
    public String avalibleBrands(String model) {
        for (String carModel : avalibleCars.keySet()) {
            ArrayList<Car> brandCars = avalibleCars.get(carModel);
            for (Car car : brandCars) {
                if (car.model.equals(model)) {
                    return car.toString();
                }


            }

        }
        return "Dany model nie został odnaleziony!";
    }


    public void addNewCar(Car car) {
        if(avalibleCars.containsKey(car.brand)) {
            avalibleCars.get(car.brand).add(car);
        }else {
            avalibleCars.put(car.brand, new ArrayList<Car>());
            avalibleCars.get(car.brand).add(car);
        }
    }
    public void buyCar(Car car) {
        if(avalibleCars.containsKey(car.brand) && !avalibleCars.get(car.brand).isEmpty()) {
            avalibleCars.get(car.brand).remove(car);
        }else System.out.println("Brak pojazdu w komisie.");
    }

}
