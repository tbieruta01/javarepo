package Second;

public class Car {
    String brand;
    String model;
    double price;
    int year;
    public Car(String brand, String model, double price, int year) {
        this.brand = brand;
        this.model = model;
        this.price = price;
        this.year = year;
    }
    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        else if(obj == null)
            return false;
        else if(getClass() != obj.getClass())
            return false;
        Car secondCar = (Car) obj;
        return(secondCar.brand.equals(this.brand) &&
                secondCar.model.equals(this.model)&&
                secondCar.price == this.price &&
                secondCar.year == this.year
        );
    }
    @Override
    public int hashCode()  {
        return brand.hashCode() + model.hashCode() + (int)(price*100) + year;
    }
}
