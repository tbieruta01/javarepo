
package com.list9;

import First.Course;
import First.Student;
import First.Teacher;
import First.University;
import Second.Car;
import Second.Dealer;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Student marian = new Student("Marian", "Nowak", 4, "Fundusze Publiczne");
        Student jan = new Student("Jan", "Jaworski", 6, "Gastronomia Profesjonalna");
        Student marianna = new Student("Marianna", "Kowalska", 7, "Zbieranie Grzybów");
        Student adrian = new Student("Andrzej", "Nowakowski", 2, "Podstawy wiązania Betonu");
        Teacher godlewski = new Teacher("Ludwik", "Godlewski");
        Teacher janiak = new Teacher("Krzysztof", "Janiak");
        Course tluczenieGarow = new Course("Tłuczenie Garów", godlewski, "TLG");
        Course pranieisuszenie = new Course("Pranie i suszenie", janiak, "PriS");
        pranieisuszenie.addToStudentsList(jan);
        pranieisuszenie.addToStudentsList(marian);
        tluczenieGarow.addToStudentsList(marianna);
        tluczenieGarow.addToStudentsList(adrian);
        tluczenieGarow.addToStudentsList(marian);
        University uniwersytet = new University("Wyższa Szkoła Tłuczenia Garów");
        uniwersytet.appendToCoursesSet(tluczenieGarow);
        uniwersytet.appendToCoursesSet(pranieisuszenie);
        System.out.println(uniwersytet);
        Car mitsubishi = new Car("Mitsubishi", "Colt", 23000.66, 2012);
        Car toyota = new Car("Toyota", "Yaris", 29000.55, 2014);
        Car tata = new Car("Tata", "Nano", 32032.34, 2014);
        Car volkswagen = new Car("VolksWagen", "Golf", 43564.45,2016 );
        Dealer Franek = new Dealer();
        Franek.addNewCar(mitsubishi);
        Franek.addNewCar(toyota);
        Franek.addNewCar(tata);
        Franek.addNewCar(volkswagen);
        boolean whyTimeFlowsSoFast = true;
        Scanner scanner = new Scanner(System.in);
        while(whyTimeFlowsSoFast) {
            System.out.println("Czego pragniesz dokonać?");
            System.out.println(" A)Dodaj auto \n B)Kup Auto\n W)Wyświetl samochody \n X)Wyjdź");
            char input = scanner.next().toLowerCase().charAt(0);
            switch (input) {
                case 'a':
                    System.out.println("Marka, \nModel, \nCena, \nRocznik");
                    Franek.addNewCar(new Car(scanner.next(), scanner.next(),scanner.nextDouble(),scanner.nextInt()));
                    break;
                case 'b':

                    System.out.println("Marka, \nModel, \nCena, \nRocznik");
                    Franek.buyCar(new Car(scanner.next(), scanner.next(),scanner.nextDouble(),scanner.nextInt()));
                    break;
                case 'w':
                    System.out.println("Podaj model, który chcesz wyszukać");
                    Franek.avalibleBrands(scanner.next());
                    break;
                case 'x':
                    whyTimeFlowsSoFast = false;
                    break;
                default:
                    System.out.println("Sorry, zła opcja :c");
                    break;
            }


        }
    }
}
