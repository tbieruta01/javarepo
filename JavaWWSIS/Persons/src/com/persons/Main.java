package com.persons;




import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

	Person Jacob = new Person("Jacob", "Else", "ExampleAddress");
	Jacob.printPersonalData();

	Scanner scanner = new Scanner(System.in);

	System.out.println("Proszę podać a:");
		int a = scanner.nextInt();

	System.out.println("Proszę podać b:");
		int b = scanner.nextInt();

	System.out.println("Proszę podać c:");
		int c = scanner.nextInt();

	QuadraticEquation calculateRoots = new QuadraticEquation(a,b,c);

	calculateRoots.printResult();
	Company exampleCompany = new Company();
	chooseOption(exampleCompany);

    }
    static char scanOption() {
		System.out.println("\nProszę wybrać opcję: ");
		System.out.println("A: Dodaj pracownika");
		System.out.println("D: Usuń pracownika");
		System.out.println("E: Wyjdź");
		Scanner scanner = new Scanner(System.in);
		return scanner.next().charAt(0);
	}
    static void chooseOption (Company company){
    	boolean isActive = true;
    	while(isActive){

		switch (scanOption()) {
			case 'A':
				company.addEmployee();
				break;
			case 'D':
				company.deleteEmployee();
				break;
			case 'E': isActive = false;
				break;
			default:
				System.out.println("Niepoprawna opcja.");
		}
		}

	}

}
