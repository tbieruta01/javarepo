package com.persons;


public class Person {
    String name;
    String surname;
    String address;


    Person(String name, String surname, String address) {
        this.name = name;
        this.surname = surname;
        //this.address = address;
        Address longAddress = new Address();
        this.address = longAddress.toString();
    }
    Person() {
        this.name = "Name";
        this.surname = "Surname";
        Address longAddress = new Address();
        this.address = longAddress.toString();

        //this.address = "Address";
    }

    public String toString() {
        return "Name: " + name + "\nSurname: " + surname + "\nAddress: "+ address ;

    }

    void printPersonalData() {
        System.out.println(toString());
    }

}
