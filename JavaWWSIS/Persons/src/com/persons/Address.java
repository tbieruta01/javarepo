package com.persons;


public class Address {
    String street;
    String buildingNumber;
    String flatNumber;

    Address(String street, String buildingNumber, String flatNumber){
        this.street = street;
        this.buildingNumber = buildingNumber;
        this.flatNumber = flatNumber;
    }
    Address(){
        this.street = "Example";
        this.buildingNumber = "0";
        this.flatNumber = "0";
    }

    public String toString() {
        return street + " " + buildingNumber + "/" + flatNumber;
    }
}
