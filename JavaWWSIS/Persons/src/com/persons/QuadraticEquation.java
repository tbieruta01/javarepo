package com.persons;


import java.util.ArrayList;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class QuadraticEquation {

    double a;
    double b;
    double c;
    ArrayList<Double> roots;

    QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
        roots = new ArrayList<>();
    }

    void calculate() {
        double delta = Math.pow(b,2) - (4 * a *c);


        if(delta >=0) {
            roots.add((-b - sqrt(delta)) / (2 * a));
            roots.add((-b + sqrt(delta)) / (2 * a));
        }



    }
    void printResult(){
        if(!roots.isEmpty()) {
            System.out.println("Pierwiastki równania kwadratowego to: " + roots);
        }
        else System.out.println("Brak pierwiastków równania kwadratowego");
    }
}
