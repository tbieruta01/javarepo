package com.persons;


public class Employee {
    Employee() {
        name = "Jan";
        surname = "Kowalski";
    }
    Employee(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
    String name;
    String surname;
}
