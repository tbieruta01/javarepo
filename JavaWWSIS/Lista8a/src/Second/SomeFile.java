package Second;

import java.io.*;
import java.util.Scanner;

public class SomeFile {
    private String fileName;
    private String filePath;
    public SomeFile() {

    }
    public SomeFile(String filePath, String fileName) {
        this.fileName = fileName;
        this.filePath = filePath;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public void setFilePath(String filePath){
        this.filePath = filePath;
    }
    public String getFileName(){
        return fileName;
    }
    public String getFilePath() {
        return filePath;
    }
    public boolean createFile() throws IOException {
        File file = new File(filePath + fileName);
        try {
            file.createNewFile();
            return true;
        }catch(IOException e){
            System.out.println("Niestety, plik nie został utworzony!\n Powód: " + e );
            return false;
        }
    }
    public void readFromFile() throws FileNotFoundException {
        try {
            Scanner scanner = new Scanner(new File(filePath + fileName));
            while(scanner.hasNextLine()){
                System.out.println(scanner.nextLine());
            }
            scanner.close();
        }catch(FileNotFoundException e) {
            System.out.println("Plik nie został znaleziony!");
        }
    }
    public void writeToFile(String s) throws IOException {
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(filePath + fileName);
        }
        catch (FileNotFoundException e ) {
            System.out.println("Tworzenie nowego pliku");
            createFile();
           printWriter = new PrintWriter(filePath + fileName);
        }
        finally {
            printWriter.println(s);
            printWriter.close();
        }
    }
    public void appendToFile(String s) throws IOException {
        File file = new File(filePath + fileName);
        FileWriter fileWriter = new FileWriter(file,true);
        try (BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            try {

                bufferedWriter.write(s);
            } catch (IOException e) {
                System.out.println("Wystąpił problem \n" + e);
            } finally {
                bufferedWriter.close();
            }
        }
    }
}

