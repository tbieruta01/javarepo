package First;


public class AnimalOwner {
    String name;
    String surname;
    Animal animal;

    public AnimalOwner (String name, String surname, Animal animal) {
        this.name = name;
        this.surname = surname;
        this.animal = animal;
    }
    @Override
    public int hashCode() {
        return name.hashCode() + surname.hashCode() + animal.hashCode();
    }

    @Override
    public String toString() {
        return (name + " " + surname + " jest właścicielem zwierzęcia o danych: \n" + animal);
    }

    @Override
    public boolean equals(Object b){
        if(this == b){
            return true;
        }else if(b == null) {
            return false;
        }else if(getClass() != b.getClass()) {
            return false;
        }
        AnimalOwner secondAnimalO = (AnimalOwner) b;
        return((secondAnimalO.name.equals(this.name))&&(secondAnimalO.surname.equals(this.surname))&&(secondAnimalO.animal.equals(this.animal)));
    }
}
