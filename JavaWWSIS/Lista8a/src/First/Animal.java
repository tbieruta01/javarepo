package First;

public class Animal {
    String name;
    int age;
    boolean isWild;
    public Animal(String name, int age, boolean isWild) {
        this.name = name;
        this.age = age;
        this.isWild = isWild;
    }
    @Override
    public int hashCode() {
        return name.hashCode() + age;
    }
    @Override
    public String toString() {
        return ("Imię " + name + " Wiek: " + age + "Czy jest dzikie? " + isWild);
    }
    @Override
    public boolean equals(Object b){
        if(this == b){
            return true;
        }else if(b == null) {
            return false;
        }else if(getClass() != b.getClass()) {
            return false;
        }
        Animal secondAnimal = (Animal) b;
        return((secondAnimal.name.equals(this.name))&&(secondAnimal.age == this.age)&&(secondAnimal.isWild == this.isWild));

    }
}
