package com.list8;

import First.Animal;
import First.AnimalOwner;
import Second.SomeFile;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Animal animal = new Animal("Ken", 13, true);
        Animal animal1 = new Animal("Lel", 33, false);
        System.out.println(animal.equals(animal1));
        System.out.println(animal.hashCode());
        AnimalOwner animalOwner = new AnimalOwner("Anna", "Kowalska", animal);
        AnimalOwner animalOwner1 = new AnimalOwner("Jan", "Kowalski", animal1);
        System.out.println(animalOwner.equals(animalOwner1));
        System.out.println("Proszę wybrać plik:");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nazwa pliku");
        String filename = scanner.next();
        System.out.println("Ścieżka (jeżeli w bieżącym katalogu, zostaw pusty");
        String filepatch = scanner.next();
        SomeFile someFile = new SomeFile(filepatch, filename);
        someFile.createFile();
        boolean conti = true;
        while(conti) {
            System.out.println("Proszę wybrać opcję \n W) Nadpisanie \n A) Dołączenie \n R) Odczyt \n C) Zmień plik \n V) Wyświetl plik \n X) Wyjście");
            char option = scanner.next().toLowerCase().charAt(0);
            switch (option) {
                case 'w':
                    System.out.println("Tekst do wpisania:");
                    someFile.writeToFile(scanner.next());
                    break;
                case 'a':
                    System.out.println("Tekst do wpisania:");
                    someFile.appendToFile(scanner.next());
                    break;
                case 'r':
                    someFile.readFromFile();
                    break;
                case 'c':
                    System.out.println("Proszę podać ścieżkę");
                    someFile.setFilePath(scanner.next());
                    System.out.println("Proszę podać nazwę pliku");
                    someFile.setFilePath(scanner.next());
                    break;
                case 'v':
                    System.out.println(someFile.getFilePath() + someFile.getFileName());
                    break;
                case 'x':
                    conti = false;
                    break;
                default:
                    System.out.println("Nie wybrano opcji!");


            }
        }
    }
}
